function savepdf(name)
% savepdf(name)
% Function to save current figure (and its current size on the
% screen) to a pdf and eps
% System calls assume a unix environment, but should be update-able
% for others
set(gcf,'paperpositionmode','auto');
print tmp.eps -depsc2
! ps2pdf tmp.eps tmp.pdf
unix(['pdfcrop tmp.pdf ' name '.pdf']);
unix(['cp tmp.eps ' name '.eps']);